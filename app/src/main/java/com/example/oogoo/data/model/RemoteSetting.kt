package com.example.oogoo.data.model


data class RemoteSetting(
    var id: Int,
    var stgKey: String,
    var stgValue: String,
    var slug: String
)
