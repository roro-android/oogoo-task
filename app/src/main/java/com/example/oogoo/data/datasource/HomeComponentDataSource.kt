package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteHomeComponent
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeComponentDataSource {
    @GET("api/homeComponent")
    suspend fun getHomeComponent(@Query("page") page: Int, @Query("isTopman") isTopman: Boolean)
            : BaseWrapper<List<RemoteHomeComponent>>
}