package com.example.oogoo.data.reository.brands

import com.example.oogoo.data.datasource.SelectBrandsDataSource
import javax.inject.Inject

class BrandsRepo @Inject constructor(
    private val selectBrandsDataSource: SelectBrandsDataSource
) {
    suspend fun getAllBrands() =
        selectBrandsDataSource.getAllBrands().data

}