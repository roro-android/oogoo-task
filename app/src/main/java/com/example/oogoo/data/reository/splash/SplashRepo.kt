package com.example.oogoo.data.reository.splash

import com.example.oogoo.data.datasource.SplashDataSource
import javax.inject.Inject

class SplashRepo @Inject constructor(
    private val splashDataSource: SplashDataSource
) {
    suspend fun getSetting() = splashDataSource.getSetting().data
}