package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteAllBrand
import retrofit2.http.GET

interface SelectBrandsDataSource
{
    @GET ("api/assets/allAdsBrands")
    suspend fun getAllBrands() : BaseWrapper<List<RemoteAllBrand>>
}