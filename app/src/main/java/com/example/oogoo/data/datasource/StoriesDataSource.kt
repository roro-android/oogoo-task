package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteStores
import retrofit2.http.GET

interface StoriesDataSource {

    @GET("api/stories")
    suspend fun getStories(): BaseWrapper<List<RemoteStores>>
}