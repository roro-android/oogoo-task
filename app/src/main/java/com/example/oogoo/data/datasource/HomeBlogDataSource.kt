package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteHomeBlog
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeBlogDataSource {
    @GET("api/homeBlog")
    suspend fun getHomeBlog( @Query("page")page: Int) : BaseWrapper<List<RemoteHomeBlog>>

}