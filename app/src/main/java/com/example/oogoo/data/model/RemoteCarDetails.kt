package com.example.oogoo.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.Year
import java.util.*

@Parcelize
data class RemoteCarDetails(
    var id: Int? = null,
    var phonePublic: String? = null,
    var namePublic: String? = null,
    var sellingPrice: String? = null,
    var description: String? = null,
    var car_status: String? = null,
    var kilometers: String? = null,
    var brand: Brand? = null,
    var carModel: CarModel? = null,
    var subModel: SubModel? = null,
    var year: YearModel? = null,
    var adAttachments: List<AdAttachment>? = null,
    var adPackageSelected: List<AdPackageSelected>? = null,
    var tradeNow: Boolean? = null,
    var isFavourites: Boolean? = null,
    var postedAt: String? = null,
    var typeAd: String? = null

) : Parcelable

@Parcelize
data class Brand(
    var id: Int? = null,
    var name: String? = null
) : Parcelable

@Parcelize
data class YearModel(
    var id: Int? = null,
    var name: String? = null
) : Parcelable

@Parcelize

data class AdPackageSelected(
    var id: Int? = null,
    var featuresId: String? = null,
    var packageId: String? = null
) : Parcelable

@Parcelize

data class SubModel(
    var id: Int? = null,
    val name: String? = null

) : Parcelable