package com.example.oogoo.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AdAttachment(
    var id: Int? = null,
    var name: String? = null,
    var type: AdAttachmentType? = null
) : Parcelable

