package com.example.oogoo.data.reository.cardetails

import com.example.oogoo.data.datasource.CarDetailsDataSource
import com.example.oogoo.data.model.RemoteCarDetails
import javax.inject.Inject

class CarDetailsRepo @Inject constructor(
    private val carDetailsDataSource: CarDetailsDataSource) {

    suspend fun getCarDetails(id:String,typeAd: String) :RemoteCarDetails {

        return carDetailsDataSource.getAdDetails(id, typeAd).data
    }
}