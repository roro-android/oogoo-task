package com.example.oogoo.data.model.common


data class RemoteAllAssets(
    var types: List<Types>
)

data class Types(
    var model: String,
    var slug: String,
    var image: String,
    var description: String,
    var subTitle: String
)