package com.example.oogoo.data.model

    data class RemoteStores(
        var id: Int,
        var adName: String,
        var sellingPrice: String,
        var brandLogo: String,
        var adId: Int,
        var adLogo: String,
        var story: String,
        var timeAgo: String,
        var typeAd: String,
        var createdAt: String,
        var isViewed: Boolean = false
    )

