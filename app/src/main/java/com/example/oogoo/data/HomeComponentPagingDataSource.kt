package com.example.oogoo.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.bumptech.glide.load.HttpException
import com.example.oogoo.data.datasource.HomeComponentDataSource
import com.example.oogoo.data.model.RemoteHomeComponent
import com.example.oogoo.data.reository.home.HomeRepo
import java.io.IOException


class HomeComponentPagingDataSource(
    private val homeRepo: HomeRepo,
) : PagingSource<Int, RemoteHomeComponent>() {

    override fun getRefreshKey(state: PagingState<Int, RemoteHomeComponent>): Int? {
        return 1
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RemoteHomeComponent> {
        val page = params.key ?: 1

        return try {
            val list = homeRepo.getHomeComponent(page)

            LoadResult.Page(
                data = list,
                prevKey = null,
                nextKey = if (list.isNotEmpty()) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}

