package com.example.oogoo.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class RemoteAllBrand(
    var id: Int,
    var nameEn: String,
    var nameAr: String,
    var logo: String,
    var name: String,
    var carModels: List<CarModel>,
    var agencyBrands: List<AgencyBrands>
)

@Parcelize
data class CarModel(
    var id: Int,
    var name: String? = null,
    var modelName: String? = null,
    var productionDiscontinued: Int,
) : Parcelable

data class AgencyBrands(
    var quarterDepreciationRate: Int,
    var quarterYears: Int,
    var showPopularity: Int
)

