package com.example.oogoo.data.reository.ads

import com.example.oogoo.data.datasource.AdsDataSource
import com.example.oogoo.data.model.RemoteAds
import javax.inject.Inject

class AdsRepo @Inject constructor(
    private val adsDataSource: AdsDataSource
) {
    suspend fun getAds() : ArrayList<RemoteAds> {
       return adsDataSource.getAds().data as ArrayList<RemoteAds>
    }


}