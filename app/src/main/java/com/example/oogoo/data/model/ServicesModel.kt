package com.example.oogoo.data.model

data class ServicesModel(
    var id: Int,
    var image: Int,
    var name: String
) {

}