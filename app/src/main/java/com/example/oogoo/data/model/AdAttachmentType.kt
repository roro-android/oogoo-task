package com.example.oogoo.data.model

import com.squareup.moshi.Json

enum class AdAttachmentType {
    image,
    video,
    audio
}