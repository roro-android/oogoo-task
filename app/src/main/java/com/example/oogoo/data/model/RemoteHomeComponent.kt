package com.example.oogoo.data.model

data class RemoteHomeComponent(

    var id: String,
    var agencyBrandId: Int,
    var agencyId: Int,
    var brandId: Int,
    var body: String,
    var createdDate: String,
    var externalUrl: String,
    var officeId: Int,
    var slug: String,
    var thumbnail: String,
    var title: String,
    var isTopman : Boolean,
    var type: String,
    var promotionalSlider: Boolean,
    )
