package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteCarDetails
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CarDetailsDataSource {
    @GET("api/ads/{adId}")
    suspend fun getAdDetails(
        @Path("adId") adId: String,
        @Query("type") type: String
    ): BaseWrapper<RemoteCarDetails>
}