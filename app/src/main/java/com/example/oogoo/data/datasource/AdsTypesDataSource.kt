package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.common.RemoteAllAssets
import retrofit2.http.GET

interface AdsTypesDataSource {
    @GET("api/assets/getAllAssets")
    suspend fun getAdsTypes(): BaseWrapper<RemoteAllAssets>

}