package com.example.oogoo.data.reository.home

import com.example.oogoo.data.datasource.*
import com.example.oogoo.data.model.RemoteHomeComponent
import javax.inject.Inject

class HomeRepo @Inject constructor(
    private val storiesDataSource: StoriesDataSource,
    private val showroomDataSource: ShowroomDataSource,
    private val homeComponentDataSource: HomeComponentDataSource,
    private val adsTypesDataSource: AdsTypesDataSource,
    private val homeBlogDataSource: HomeBlogDataSource,

    ) {
    suspend fun getStories() =
        storiesDataSource.getStories().data

    suspend fun getBrands() =
        showroomDataSource.getBrands().data

    suspend fun getHomeComponent(page: Int): List<RemoteHomeComponent> =
        homeComponentDataSource.getHomeComponent(page, false).data

    suspend fun getAllAdsTypes() =
        adsTypesDataSource.getAdsTypes().data.types

    suspend fun getHomeBlog(page: Int) =
        homeBlogDataSource.getHomeBlog(page).data

}