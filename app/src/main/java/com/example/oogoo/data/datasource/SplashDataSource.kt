package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteSetting
import retrofit2.http.GET

interface SplashDataSource {

    @GET("api/mobileSetting")
    suspend fun getSetting() : BaseWrapper<List<RemoteSetting>>
}