package com.example.oogoo.data.model


data class RemoteHomeBlog(
    var id: Int,
    var brandId: Int,
    var carModelId: Int,
    var isHome: Boolean,
    var isExplore: Boolean,
    var title: String,
    var author: String,
    var content: String,
    var date: String,
    var bannerURL: String,
    var slug: String,
    var avatar: String,

    )