package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteShowRoom
import retrofit2.http.GET

interface ShowroomDataSource {
    @GET ("api/agency/getHomeShowrooms")
    suspend fun getBrands() : BaseWrapper<List<RemoteShowRoom>>
}