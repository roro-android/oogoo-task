package com.example.oogoo.data.model

data class RemoteShowRoom(
    var id: Int,
    var logo: String,
    var brandName: String,
    var brandNameEn: String,
    var brandNameAr: String,
    var brandId: Int,
    var name: String,
    var cover: String,
    var showroomCover: String
)
