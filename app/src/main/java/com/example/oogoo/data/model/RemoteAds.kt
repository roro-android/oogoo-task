package com.example.oogoo.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RemoteAds(
    var id: Int,
    var brandName: String,
    var modelName: String,
    var year: String,
    var sellingPrice: String,
    var kilometers: String,
    var thumbnail: String,
    var typeAd: String,
    var isPaid: Boolean,
    var hasVideo: Boolean,
    var hasAudio: Boolean,
    var NumberOfImages: Int,
    var deepLink: String
) : Parcelable
