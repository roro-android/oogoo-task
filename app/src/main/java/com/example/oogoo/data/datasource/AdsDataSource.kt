package com.example.oogoo.data.datasource

import com.example.oogoo.common.BaseWrapper
import com.example.oogoo.data.model.RemoteAds
import retrofit2.http.GET

interface AdsDataSource {
    @GET("api/ads")
    suspend fun getAds(): BaseWrapper<List<RemoteAds>>
}