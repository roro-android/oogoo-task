package com.example.oogoo.common

data class BaseWrapper<T>(
    val data: T
)
