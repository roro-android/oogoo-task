package com.example.oogoo.common

import android.app.Application
import com.example.oogoo.ui.cardetails.CarImageLoadingService
import dagger.hilt.android.HiltAndroidApp
import ss.com.bannerslider.Slider

@HiltAndroidApp
class CarApplication : Application() {


    override fun onCreate() {
        super.onCreate()

        Slider.init(CarImageLoadingService(this))
    }


}