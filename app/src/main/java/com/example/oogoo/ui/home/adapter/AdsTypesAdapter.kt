package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.common.Types
import com.example.oogoo.databinding.RowAdsTypesHomeBinding

class AdsTypesAdapter : ListAdapter<Types, AdsTypesAdapter.TypesViewHolder>(
    DiffCallback()
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TypesViewHolder {
        val binding =
            RowAdsTypesHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TypesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TypesViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class TypesViewHolder(val binding: RowAdsTypesHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: Types) {
            binding.model = model
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Types>() {
        override fun areItemsTheSame(oldItem: Types, newItem: Types) =
            oldItem.model == newItem.model

        override fun areContentsTheSame(
            oldItem: Types,
            newItem: Types
        ) =
            oldItem == newItem
    }


}