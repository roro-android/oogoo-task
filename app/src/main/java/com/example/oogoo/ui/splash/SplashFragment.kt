package com.example.oogoo.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.oogoo.R
import com.example.oogoo.core.constant.Constant
import com.example.oogoo.data.model.RemoteSetting
import com.example.oogoo.databinding.FragmentSplashBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : Fragment() {

    private lateinit var binding: FragmentSplashBinding
    private val viewModel: SplashViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleDelay()
        getSetting()
        initObserve()

    }

    private fun getSetting() {
        viewModel.getSetting()
    }

    private fun initObserve() {
        viewModel.successSettingsLiveData.observe(viewLifecycleOwner, ::handleSuccess)
    }

    private fun handleSuccess(list: List<RemoteSetting>?) {
        handleAction()
    }

    private fun handleDelay() {
        Handler(Looper.myLooper()!!).postDelayed({
        }, Constant.DELAY_MILLIS)
    }

    private fun handleAction() {
        findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
    }


}