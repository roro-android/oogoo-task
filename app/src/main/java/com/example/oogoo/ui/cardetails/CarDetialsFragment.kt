package com.example.oogoo.ui.cardetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.oogoo.R
import com.example.oogoo.data.model.AdAttachmentType
import com.example.oogoo.data.model.CarDetailsLabel
import com.example.oogoo.data.model.RemoteCarDetails
import com.example.oogoo.databinding.FragmentCarDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CarDetailsFragment : Fragment() {
    private lateinit var binding: FragmentCarDetailsBinding
    private val viewModel: CarDetailsViewModel by viewModels()
    private lateinit var detailsLabelAdapter: DetailsLabelAdapter
    private lateinit var carImageAdapter: CarImageAdapter
    private var model: RemoteCarDetails? = null
    private val labelList: ArrayList<CarDetailsLabel> = arrayListOf()

    val args: CarDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCarDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initArguments()
        initObservers()
        initAdapters()

    }

    private fun initAdapters() {
        carImageAdapter = CarImageAdapter()
        binding.recyclerViewActivityCarImage.adapter = carImageAdapter

        detailsLabelAdapter = DetailsLabelAdapter()
        binding.recyclerViewDetails.adapter = detailsLabelAdapter
    }

    private fun initArguments() {
        val data = args.featureAdArgs
        if (data != null) {
            viewModel.getCarDetails(data.id.toString(), data.typeAd)
        }
    }

    private fun initObservers() {
        viewModel.carDetailsSuccessLiveData.observe(viewLifecycleOwner, ::handleCarDetailsSuccess)


    }

    private fun handleCarDetailsSuccess(it: RemoteCarDetails) {

        binding.modelFragment = it
        labelList.add(CarDetailsLabel(getString(R.string.ad_id), it.id.toString()))
        labelList.add(CarDetailsLabel(getString(R.string.brand), it.brand?.name!!))
        labelList.add(CarDetailsLabel(getString(R.string.model_name), it.carModel?.name!!))
        labelList.add(CarDetailsLabel(getString(R.string.year), it.year?.name!!))
        labelList.add(CarDetailsLabel(getString(R.string.kilometers), it.kilometers!!))
        detailsLabelAdapter.submitList(labelList)

        carImageAdapter.submitList(it.adAttachments)

         val dataList =
        binding.sliderFeatureAds.setAdapter(SliderImageAdapter(
            it.adAttachments!!.filter {
                it.type == AdAttachmentType.image
            }.toMutableList()
        )
        )

    }
}
