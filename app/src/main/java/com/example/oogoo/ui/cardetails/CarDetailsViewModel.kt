package com.example.oogoo.ui.cardetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.oogoo.data.model.RemoteCarDetails
import com.example.oogoo.data.model.RemoteStores
import com.example.oogoo.data.reository.cardetails.CarDetailsRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CarDetailsViewModel @Inject constructor(
    private val carDetailsRepo: CarDetailsRepo
) : ViewModel() {
    private val _carDetailsLoadingLiveData = MutableLiveData<Boolean>()
    val carDetailsLoadingLiveData: LiveData<Boolean> = _carDetailsLoadingLiveData

    private val _carDetailsErrorLiveData = MutableLiveData<Throwable>()
    val carDetailsErrorLiveData: LiveData<Throwable> = _carDetailsErrorLiveData

    private val _carDetailsSuccessLiveData = MutableLiveData<RemoteCarDetails>()
    val carDetailsSuccessLiveData: LiveData<RemoteCarDetails> =
        _carDetailsSuccessLiveData

    fun getCarDetails(id: String, typeAd: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _carDetailsLoadingLiveData.postValue(true)
            try {
                val details = carDetailsRepo.getCarDetails(id, typeAd)
                _carDetailsSuccessLiveData.postValue(details)
            } catch (e: Exception) {
                _carDetailsErrorLiveData.postValue(e)
            }
            _carDetailsLoadingLiveData.postValue(false)
        }
    }
}