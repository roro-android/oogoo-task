package com.example.oogoo.ui.cardetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.AdAttachment
import com.example.oogoo.databinding.RowCarDetailsBinding

class CarImageAdapter :
    androidx.recyclerview.widget.ListAdapter<AdAttachment, CarImageAdapter.ImageViewHolder>(
        DiffCallback()
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val binding = RowCarDetailsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ImageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    class ImageViewHolder(val binding: RowCarDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: AdAttachment) {
            binding.x = model
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<AdAttachment>() {
        override fun areItemsTheSame(oldItem: AdAttachment, newItem: AdAttachment) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: AdAttachment,
            newItem: AdAttachment
        ) =
            oldItem == newItem
    }

}