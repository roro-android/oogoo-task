package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.ServicesModel
import com.example.oogoo.databinding.RowServicesBoxBinding

class ServicesAdapter(private val listener: OnItemClickListener) :
    ListAdapter<ServicesModel, ServicesAdapter.ServicesViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServicesViewHolder {
        val binding = RowServicesBoxBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ServicesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ServicesViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.binding.btnServices.setOnClickListener {
            listener.onServicesClick(position)
        }
    }

    class ServicesViewHolder(val binding: RowServicesBoxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: ServicesModel) {
            binding.model = model
            binding.executePendingBindings()
        }
    }

    interface OnItemClickListener {
        fun onServicesClick(position: Int)
    }

    class DiffCallback : DiffUtil.ItemCallback<ServicesModel>() {
        override fun areItemsTheSame(oldItem: ServicesModel, newItem: ServicesModel) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: ServicesModel,
            newItem: ServicesModel
        ) =
            oldItem == newItem
    }
}

