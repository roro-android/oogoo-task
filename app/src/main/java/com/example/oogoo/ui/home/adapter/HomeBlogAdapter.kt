package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.RemoteHomeBlog
import com.example.oogoo.databinding.RowHomeBlogBinding

class HomeBlogAdapter : ListAdapter<RemoteHomeBlog, HomeBlogAdapter.BlogViewHolder>(
        DiffCallback()
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BlogViewHolder {
        val binding = RowHomeBlogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BlogViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
      holder.bind(getItem(position))
    }

    inner class BlogViewHolder(val binding: RowHomeBlogBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model:RemoteHomeBlog){
            binding.model=model

        }
    }

    class DiffCallback : DiffUtil.ItemCallback<RemoteHomeBlog>() {
        override fun areItemsTheSame(oldItem: RemoteHomeBlog, newItem: RemoteHomeBlog) =
            oldItem.brandId == newItem.brandId

        override fun areContentsTheSame(
            oldItem: RemoteHomeBlog,
            newItem: RemoteHomeBlog
        ) =
            oldItem == newItem
    }


}