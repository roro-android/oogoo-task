package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.RemoteShowRoom
import com.example.oogoo.databinding.RowBrandHomeBinding

class BrandAdapter : ListAdapter<RemoteShowRoom, BrandAdapter.ShowroomViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowroomViewHolder {
        val binding =
            RowBrandHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShowroomViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShowroomViewHolder, position: Int) {
        val currentItem= getItem(position)
        holder.bind(currentItem)
    }


    class ShowroomViewHolder(val binding: RowBrandHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(model : RemoteShowRoom){
                binding.model=model
            }
    }

    class DiffCallback : DiffUtil.ItemCallback<RemoteShowRoom>() {
        override fun areItemsTheSame(oldItem: RemoteShowRoom, newItem: RemoteShowRoom) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: RemoteShowRoom,
            newItem: RemoteShowRoom
        ) =
            oldItem == newItem
    }
}