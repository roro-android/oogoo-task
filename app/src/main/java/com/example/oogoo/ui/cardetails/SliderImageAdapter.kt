package com.example.oogoo.ui.cardetails

import com.example.oogoo.data.model.AdAttachment
import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class SliderImageAdapter (private val images : List<AdAttachment>) : SliderAdapter() {
    override fun getItemCount(): Int =images.size

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder) {
        imageSlideViewHolder.bindImageSlide(images[position].name?: "")
    }


}