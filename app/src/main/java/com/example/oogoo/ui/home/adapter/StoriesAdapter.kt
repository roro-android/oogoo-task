package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.RemoteStores
import com.example.oogoo.databinding.RowStoriesHomeBinding

class StoriesAdapter : ListAdapter<RemoteStores, StoriesAdapter.StoriesViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoriesViewHolder {
        val binding = RowStoriesHomeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return StoriesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoriesViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)

    }

    class StoriesViewHolder(val binding: RowStoriesHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: RemoteStores) {
            binding.model = model
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<RemoteStores>() {
        override fun areItemsTheSame(oldItem: RemoteStores, newItem: RemoteStores) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: RemoteStores,
            newItem: RemoteStores
        ) =
            oldItem == newItem
    }
}

