package com.example.oogoo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.oogoo.R
import com.example.oogoo.data.model.*
import com.example.oogoo.data.model.common.Types
import com.example.oogoo.databinding.FragmentHomeBinding
import com.example.oogoo.ui.ads.AdsAdapter
import com.example.oogoo.ui.home.adapter.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment(), ServicesAdapter.OnItemClickListener,
    AdsAdapter.OnItemClickListener {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var storiesAdapter: StoriesAdapter
    private lateinit var servicesAdapter: ServicesAdapter
    private lateinit var adsAdapter: AdsAdapter
    private lateinit var brandAdapter: BrandAdapter
    private lateinit var homeComponentAdapter: HomeComponentAdapter
    private lateinit var adsTypesAdapter: AdsTypesAdapter
    private lateinit var homeBlogAdapter: HomeBlogAdapter
    private var services: ArrayList<ServicesModel> = arrayListOf()
    private val isValuation = true
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycle.addObserver(viewModel)
        initAdapters()
        initObserves()
        fillServicesData()


    }

    private fun initAdapters() {
        storiesAdapter = StoriesAdapter()
        binding.recyclerViewStores.adapter = storiesAdapter

        servicesAdapter = ServicesAdapter(this)
        binding.recyclerViewServicesBox.adapter = servicesAdapter

        adsAdapter = AdsAdapter(this)
        binding.recyclerFeatureAds.adapter = adsAdapter

        brandAdapter = BrandAdapter()
        binding.recyclerViewBrands.adapter = brandAdapter

        homeComponentAdapter = HomeComponentAdapter()
        binding.recyclerViewPaidAds.adapter = homeComponentAdapter

        adsTypesAdapter = AdsTypesAdapter()
        binding.recyclerViewAdsTypes.adapter = adsTypesAdapter

        homeBlogAdapter = HomeBlogAdapter()
        binding.recyclerViewBlog.adapter = homeBlogAdapter

    }

    private fun fillServicesData() {
        services.add(ServicesModel(1, 1, getString(R.string.car_valuation)))
        services.add(ServicesModel(2, 1, getString(R.string.sell_now)))
        services.add(ServicesModel(3, 1, getString(R.string.place_an_add)))
        services.add(ServicesModel(4, 1, getString(R.string.personal_assistent)))
        servicesAdapter.submitList(services)

    }

    private fun initObserves() {
        viewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)
        viewModel.adsSuccessLiveData.observe(viewLifecycleOwner, ::handleAdsSuccess)
        viewModel.storiesSuccessLiveData.observe(viewLifecycleOwner, ::handleStoresSuccess)
        viewModel.brandsSuccessLiveData.observe(viewLifecycleOwner, ::handleBrandsSuccess)
        viewModel.successHomeComponentLiveData.observe(
            viewLifecycleOwner,
            ::handleHomeComponentSuccess
        )

        viewModel.successAdsTypesLiveData.observe(viewLifecycleOwner, ::handleAdsTypesSuccess)
        viewModel.successHomeBLogLiveData.observe(viewLifecycleOwner, ::handleHomeBLogSuccess)

    }

    private fun handleHomeBLogSuccess(list: List<RemoteHomeBlog>?) {
        homeBlogAdapter.submitList(list)
    }

    private fun handleAdsTypesSuccess(list: List<Types>) {
        adsTypesAdapter.submitList(list)
    }

    private fun handleAdsSuccess(list: List<RemoteAds>?) {
        adsAdapter.submitList(list)
    }

    private fun handleStoresSuccess(list: List<RemoteStores>) {
        storiesAdapter.submitList(list)
    }

    private fun handleBrandsSuccess(list: List<RemoteShowRoom>?) {
        brandAdapter.submitList(list)
    }

    private fun handleHomeComponentSuccess(list: List<RemoteHomeComponent>) {
        lifecycleScope.launch {
            viewModel.getHomeComponent().collect {
                homeComponentAdapter.submitData(it)
            }
        }

    }

    private fun handleLoading(isShow: Boolean) {
        binding.progress.isVisible = isShow
    }


    override fun onServicesClick(position: Int) {
        when (position) {
            ROW_VALUATION -> {
                findNavController().navigate(
                    R.id.action_homeFragment_to_selectBrandFragment,
                    bundleOf("isValuation" to isValuation)
                )
            }

            ROW_SELLER -> {
                findNavController().navigate(
                    R.id.action_homeFragment_to_selectBrandFragment,
                    bundleOf("isValuation" to !isValuation)
                )
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

    }

    companion object {
        const val ROW_VALUATION = 0
        const val ROW_SELLER = 1
    }

    override fun onAdsClick(model: RemoteAds) {
        val action = HomeFragmentDirections.actionHomeFragmentToCarDetailsFragment(model)
        findNavController().navigate(action)
    }


}





