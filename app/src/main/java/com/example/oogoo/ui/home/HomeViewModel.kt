package com.example.oogoo.ui.home

import androidx.lifecycle.*
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.oogoo.data.HomeComponentPagingDataSource
import com.example.oogoo.data.model.*
import com.example.oogoo.data.model.common.Types
import com.example.oogoo.data.reository.ads.AdsRepo
import com.example.oogoo.data.reository.home.HomeRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeRepo: HomeRepo,
    private val adsRepo: AdsRepo
) : ViewModel(), LifecycleObserver {

    private val _loadingLiveData = MutableLiveData<Boolean>()
    val loadingLiveData: LiveData<Boolean> = _loadingLiveData

    private val _errorLiveData = MutableLiveData<Throwable>()
    val errorLiveData: LiveData<Throwable> = _errorLiveData

    private val _storiesSuccessLiveData = MutableLiveData<List<RemoteStores>>()
    val storiesSuccessLiveData: LiveData<List<RemoteStores>> =
        _storiesSuccessLiveData

    private val _brandsSuccessLiveData = MutableLiveData<List<RemoteShowRoom>>()
    val brandsSuccessLiveData: LiveData<List<RemoteShowRoom>> =
        _brandsSuccessLiveData

    private val _adsSuccessLiveData = MutableLiveData<List<RemoteAds>>()
    val adsSuccessLiveData: LiveData<List<RemoteAds>> =
        _adsSuccessLiveData

    private val _successHomeComponentLiveData = MutableLiveData<List<RemoteHomeComponent>>()
    val successHomeComponentLiveData: LiveData<List<RemoteHomeComponent>> =
        _successHomeComponentLiveData

    private val _successAdsTypesLiveData = MutableLiveData<List<Types>>()
    val successAdsTypesLiveData: LiveData<List<Types>> = _successAdsTypesLiveData

    private val _successHomeBLogLiveData = MutableLiveData<List<RemoteHomeBlog>>()
    val successHomeBLogLiveData: LiveData<List<RemoteHomeBlog>> = _successHomeBLogLiveData

    private val _successSelectBrandsLiveData = MutableLiveData<List<RemoteAllBrand>>()
    val successSelectBrandsLiveData: LiveData<List<RemoteAllBrand>> = _successSelectBrandsLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        getStories()
        getAds()
        getBrands()
        getHomeComponent()
        getAdsTypes()
        getHomeBlog()
    }

    private fun getStories() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingLiveData.postValue(true)
            try {
                val stories = homeRepo.getStories()
                _storiesSuccessLiveData.postValue(stories)
            } catch (e: Exception) {
                _errorLiveData.postValue(e)
            }
            _loadingLiveData.postValue(false)
        }
    }

    private fun getAds() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingLiveData.postValue(true)
            try {
                val ads = adsRepo.getAds()
                _adsSuccessLiveData.postValue(ads)
            } catch (e: Exception) {
                _errorLiveData.postValue(e)
            }
            _loadingLiveData.postValue(false)
        }
    }

    private fun getBrands() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingLiveData.postValue(true)
            try {
                val brands = homeRepo.getBrands()
                _brandsSuccessLiveData.postValue(brands)
            } catch (e: Exception) {
                _errorLiveData.postValue(e)
            }
            _loadingLiveData.postValue(false)
        }
    }

//    private fun getHomeComponent() {
//        viewModelScope.launch(Dispatchers.IO) {
//            _loadingLiveData.postValue(true)
//            try {
//                val homeComponent = homeRepo.getHomeComponent(1, false)
//                _successHomeComponentLiveData.postValue(homeComponent)
//            } catch (e: Exception) {
//                _errorLiveData.postValue(e)
//            }
//            _loadingLiveData.postValue(false)
//        }
//    }

     fun getHomeComponent() : Flow<PagingData<RemoteHomeComponent>>{
        return Pager(
            config = PagingConfig(15),
            pagingSourceFactory = { HomeComponentPagingDataSource(homeRepo) }
        ).flow.cachedIn(viewModelScope)
    }

    private fun getAdsTypes() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingLiveData.postValue(true)
            try {
                val adsTypes = homeRepo.getAllAdsTypes()
                _successAdsTypesLiveData.postValue(adsTypes)
            } catch (e: Exception) {
                _errorLiveData.postValue(e)
            }
            _loadingLiveData.postValue(false)
        }
    }

    private fun getHomeBlog() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingLiveData.postValue(true)
            try {
                val homeBlog = homeRepo.getHomeBlog(1)
                _successHomeBLogLiveData.postValue(homeBlog)
            } catch (e: Exception) {
                _errorLiveData.postValue(e)
            }
            _loadingLiveData.postValue(false)
        }
    }


}