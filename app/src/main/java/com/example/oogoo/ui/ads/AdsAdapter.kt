package com.example.oogoo.ui.ads

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.RemoteAds
import com.example.oogoo.databinding.RowAdsHomeBinding
import com.example.oogoo.ui.ads.AdsAdapter.AdsViewHolder

class AdsAdapter(private val listener: OnItemClickListener) :
    androidx.recyclerview.widget.ListAdapter<RemoteAds, AdsViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdsViewHolder {
        val binding = RowAdsHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AdsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AdsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
        holder.itemView.setOnClickListener {
            listener.onAdsClick(currentItem)
        }
    }

    inner class AdsViewHolder(private val binding: RowAdsHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: RemoteAds) {
            binding.model = model
        }
    }

    interface OnItemClickListener {
        fun onAdsClick(model: RemoteAds)

    }
}

class DiffCallback : DiffUtil.ItemCallback<RemoteAds>() {
    override fun areItemsTheSame(oldItem: RemoteAds, newItem: RemoteAds) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: RemoteAds,
        newItem: RemoteAds
    ) =
        oldItem == newItem
}

