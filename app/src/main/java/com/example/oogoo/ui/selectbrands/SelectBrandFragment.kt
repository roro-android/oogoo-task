package com.example.oogoo.ui.selectbrands

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.oogoo.data.model.RemoteAllBrand
import com.example.oogoo.databinding.FragmentSelectBrandBinding
import com.example.oogoo.ui.home.adapter.SelectBrandAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SelectBrandFragment : Fragment() {
    private lateinit var binding: FragmentSelectBrandBinding
    private lateinit var selectBrandAdapter: SelectBrandAdapter
    private var isValuation = false
    private val brandsViewModel: BrandsViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSelectBrandBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycle.addObserver(brandsViewModel)
        initArgument()
        initAdapters()
        initObserver()

    }

    private fun initArgument() {
        isValuation = arguments?.getBoolean("isValuation",false)!!
    }

    private fun initAdapters() {
        selectBrandAdapter = SelectBrandAdapter()
        binding.recyclerViewSelectBrand.adapter = selectBrandAdapter
    }

    private fun initObserver() {
        brandsViewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)
        brandsViewModel.successSelectBrandsLiveData.observe(
            viewLifecycleOwner,
            ::handleBrandsSuccess
        )
    }

    private fun handleBrandsSuccess(list: List<RemoteAllBrand>) {
        selectBrandAdapter.submitList(list)
        //  binding.recyclerViewSelectBrand.adapter?.notifyDataSetChanged()
    }

    private fun handleLoading(isShow: Boolean) {
        binding.progress.isVisible = isShow
    }
}