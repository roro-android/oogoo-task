package com.example.oogoo.ui.selectbrands

import androidx.lifecycle.*
import com.example.oogoo.data.model.RemoteAllBrand
import com.example.oogoo.data.reository.brands.BrandsRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BrandsViewModel @Inject constructor(
    private val brandsRepo: BrandsRepo
) : ViewModel(), LifecycleObserver {

    private val _loadingLiveData = MutableLiveData<Boolean>()
    val loadingLiveData: LiveData<Boolean> = _loadingLiveData

    private val _errorLiveData = MutableLiveData<Throwable>()
    val errorLiveData: LiveData<Throwable> = _errorLiveData

    private val _successSelectBrandsLiveData = MutableLiveData<List<RemoteAllBrand>>()
    val successSelectBrandsLiveData: LiveData<List<RemoteAllBrand>> = _successSelectBrandsLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {

        getAllBrands()
    }


    fun getAllBrands() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingLiveData.postValue(true)
            try {
                val brands = brandsRepo.getAllBrands()
                _successSelectBrandsLiveData.postValue(brands)
            } catch (e: Exception) {
                _errorLiveData.postValue(e)
            }
            _loadingLiveData.postValue(false)
        }
    }
}