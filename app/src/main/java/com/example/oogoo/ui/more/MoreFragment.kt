package com.example.oogoo.ui.more

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.example.oogoo.R

class MoreFragment  : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.fragment_more, rootKey)
    }
}