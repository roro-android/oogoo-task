package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.RemoteAllBrand
import com.example.oogoo.databinding.RowSelectBrandsBinding

class SelectBrandAdapter() :
    ListAdapter<RemoteAllBrand, SelectBrandAdapter.SelectBrandsViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectBrandsViewHolder {
        val binding = RowSelectBrandsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return SelectBrandsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SelectBrandsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    class SelectBrandsViewHolder(val binding: RowSelectBrandsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: RemoteAllBrand) {
            binding.model = model
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<RemoteAllBrand>() {
        override fun areItemsTheSame(oldItem: RemoteAllBrand, newItem: RemoteAllBrand) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: RemoteAllBrand,
            newItem: RemoteAllBrand
        ) =
            oldItem == newItem
    }
}