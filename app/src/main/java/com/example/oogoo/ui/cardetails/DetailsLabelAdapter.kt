package com.example.oogoo.ui.cardetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.leanback.widget.DiffCallback
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.CarDetailsLabel
import com.example.oogoo.data.model.RemoteAllBrand
import com.example.oogoo.data.model.RemoteCarDetails
import com.example.oogoo.databinding.RowDetailsLablelBinding
import com.example.oogoo.databinding.RowSelectBrandsBinding
import com.example.oogoo.ui.cardetails.DetailsLabelAdapter.ViewHolder

class DetailsLabelAdapter :
ListAdapter<CarDetailsLabel, ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDetailsLablelBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    class ViewHolder(val binding: RowDetailsLablelBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data:CarDetailsLabel) {
            binding.model =data
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<CarDetailsLabel>() {
        override fun areItemsTheSame(oldItem: CarDetailsLabel, newItem: CarDetailsLabel) =
            oldItem.label == newItem.label

        override fun areContentsTheSame(
            oldItem: CarDetailsLabel,
            newItem: CarDetailsLabel
        ) =
            oldItem == newItem
    }
}
