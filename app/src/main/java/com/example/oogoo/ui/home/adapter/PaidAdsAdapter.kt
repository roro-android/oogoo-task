package com.example.oogoo.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.oogoo.data.model.RemoteHomeComponent
import com.example.oogoo.databinding.RowPaidAdsBinding

class HomeComponentAdapter :
    PagingDataAdapter<RemoteHomeComponent, HomeComponentAdapter.RemoteHomeComponentsViewHolder>(
        DiffCallback()
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RemoteHomeComponentsViewHolder {
        val binding = RowPaidAdsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return RemoteHomeComponentsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RemoteHomeComponentsViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    inner class RemoteHomeComponentsViewHolder(val binding: RowPaidAdsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: RemoteHomeComponent) {
            binding.model = model
            if (model.promotionalSlider == true) {
                binding.root.isClickable = false /// !model.promotionalSlider
            }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<RemoteHomeComponent>() {
        override fun areItemsTheSame(oldItem: RemoteHomeComponent, newItem: RemoteHomeComponent) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: RemoteHomeComponent,
            newItem: RemoteHomeComponent
        ) =
            oldItem == newItem
    }
}