package com.example.oogoo.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.oogoo.data.model.RemoteSetting
import com.example.oogoo.data.reository.splash.SplashRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val splashRepo: SplashRepo
) : ViewModel() {

    private val _progressLoadingLiveData = MutableLiveData<Boolean>()
    val progressLoadingLiveData: LiveData<Boolean> = _progressLoadingLiveData

    private val _successSettingsLiveData = MutableLiveData<List<RemoteSetting>?>()
    val successSettingsLiveData: LiveData<List<RemoteSetting>?> = _successSettingsLiveData

    private val _errorHandlerLiveData = MutableLiveData<Throwable>()
    val errorHandlerLiveData: LiveData<Throwable> = _errorHandlerLiveData

    fun getSetting() {
        viewModelScope.launch(Dispatchers.IO) {
            _progressLoadingLiveData.postValue(true)
            try {
                var setting = splashRepo.getSetting()
                _successSettingsLiveData.postValue(setting)
            } catch (e: Exception) {
                _errorHandlerLiveData.postValue(e)
            }
            _progressLoadingLiveData.postValue(false)
        }
    }
}