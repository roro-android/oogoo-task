package com.example.oogoo.di

import com.example.oogoo.core.constant.Constant
import com.example.oogoo.data.datasource.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Module {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    fun provideDataSource(retrofit: Retrofit) : SplashDataSource =
        retrofit.create(SplashDataSource::class.java)

    @Provides
    fun provideStoriesDataSource(retrofit: Retrofit) : StoriesDataSource =
        retrofit.create(StoriesDataSource::class.java)

    @Provides
    fun provideAdsDataSource(retrofit: Retrofit) : AdsDataSource =
        retrofit.create(AdsDataSource::class.java)

    @Provides
    fun provideBrandsDataSource(retrofit: Retrofit) : ShowroomDataSource =
        retrofit.create(ShowroomDataSource::class.java)
// bind
    @Provides
    fun provideHomeComponentDataSource(retrofit: Retrofit) : HomeComponentDataSource =
        retrofit.create(HomeComponentDataSource::class.java)

    @Provides
    fun provideAdsTypesDataSource(retrofit: Retrofit) : AdsTypesDataSource =
        retrofit.create(AdsTypesDataSource::class.java)

    @Provides
    fun provideHomeBlogDataSource(retrofit: Retrofit) : HomeBlogDataSource =
        retrofit.create(HomeBlogDataSource::class.java)

    @Provides
    fun provideSelectBrandsDataSource(retrofit: Retrofit) : SelectBrandsDataSource =
        retrofit.create(SelectBrandsDataSource::class.java)

    @Provides
    fun carDetailsDataSource(retrofit: Retrofit) : CarDetailsDataSource =
        retrofit.create(CarDetailsDataSource::class.java)
}