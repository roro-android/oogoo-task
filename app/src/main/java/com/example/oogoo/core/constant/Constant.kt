package com.example.oogoo.core.constant

class Constant {
    companion object {
        const val BASE_URL = "https://api-stg.oogoocar.com/"
        const val DELAY_MILLIS = 3000L

    }
}